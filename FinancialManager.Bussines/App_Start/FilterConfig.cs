﻿using System.Web;
using System.Web.Mvc;

namespace FinancialManager.Bussines
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
