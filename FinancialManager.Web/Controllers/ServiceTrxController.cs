﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinancialManager.Entities ;
using Newtonsoft.Json;
using System.Net;
using Newtonsoft.Json.Linq;
using FinancialManager.Web.Models;
using FinancialManager.Api.Models;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using System.Configuration;
namespace FinancialManager.Web.Controllers
{
    public class ServiceTrxController : Controller
    {
        //
        // GET: /ServiceTrx/
        public ActionResult Index()
        {
            
            return View();
        }

        // GET: /ServiceTrx/
        public ActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public ActionResult MarkFraud( int id)
        {
         
            var datos = User.Identity.GetUserId();
            Char delimiter = '|';
            String[] substrings = datos.Split(delimiter);
            var user = substrings[0];
            var tocken = substrings[1];

            var client = new WebClient();
            var server = ConfigurationManager.AppSettings["service"];
            var request = string.Format( server +@"/transaction/EditTransaction?id={0}&user={1}&tocken={2}"
                , id, user, Uri.EscapeDataString(tocken));

            var resultado = client.DownloadString(request);
            ListTransaction data = JsonConvert.DeserializeObject<ListTransaction>(resultado);
            
            return View( data.messsage.Single());
        }

        [HttpPost]
        public ActionResult Create(string accountorigen, string typeTrx, string value, string accountdestine)
        {
            var datos = User.Identity.GetUserId();
            Char delimiter = '|';
            String[] substrings = datos.Split(delimiter);
            var user = substrings[0];
            var tocken = substrings[1];
            var server = ConfigurationManager.AppSettings["service"];
            var client = new WebClient();
            var request = string.Format(server + @"/transaction/Register?accountorigen={0}&typeTrx={1}&accountdestine={2}&user={3}&tocken={4}&value={5}"
                , accountorigen, typeTrx, accountdestine, user, Uri.EscapeDataString(tocken), value);
            var resultado = client.UploadString(request, "POST");
            DtoUser userd = JsonConvert.DeserializeObject<DtoUser>(resultado);
            TempData["Success"] = "Added Successfully!";
            return RedirectToAction("Create") ;
        }

        [HttpPost]
        public ActionResult RegisterFraud(string isFraud , string id)
        {
            var datos = User.Identity.GetUserId();
            Char delimiter = '|';
            String[] substrings = datos.Split(delimiter);
            var user = substrings[0];
            var tocken = substrings[1];

            var client = new WebClient();
            var server = ConfigurationManager.AppSettings["service"];
            var request = string.Format(server+ @"/transaction/ReportFraud?idtransation={0}&isfraud={1}&user={2}&tocken={3}"
                , id,  isFraud , user, Uri.EscapeDataString(tocken));
            var resultado = client.UploadString(request, "POST");
            DtoUser userd = JsonConvert.DeserializeObject<DtoUser>(resultado);
            TempData["SuccessUpdate"] = "Updated Successfully!";
            return RedirectToAction("MarkFraud", new { id = id});
        }



        [HttpPost]
        public ActionResult getTransactions(string pagezise, string pagenumber, string isFraud = "", string dateTrx = "", string accountDest = "" )
        {

            var datos = User.Identity.GetUserId();
            Char delimiter = '|';
            String[] substrings = datos.Split(delimiter);
            var user = substrings[0];
            var tocken = substrings[1];

            var client = new WebClient();
            var server = ConfigurationManager.AppSettings["service"];
            var request = string.Format(server+ @"/transaction/GetTransactions?" +
                                    "pagezise={0}&pagenumber={1}&user={2}&tocken={3}&isFraud={4}&dateTrx={5}&accountDest={6}",
                                    pagezise, pagenumber, user, Uri.EscapeDataString(tocken), isFraud, dateTrx, accountDest);

            var resultado = client.DownloadString(request);
            var data = JsonConvert.DeserializeObject<ListTransaction >(resultado);
            return View(data);
        }


	}
}