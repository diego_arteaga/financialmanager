﻿using FinancialManager.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinancialManager.Api.Models
{
    public class ListTransaction
    {
        [JsonProperty("information")]
        public List<DtoTransaction> messsage { get; set; }
    }
}