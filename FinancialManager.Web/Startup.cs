﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FinancialManager.Web.Startup))]
namespace FinancialManager.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
