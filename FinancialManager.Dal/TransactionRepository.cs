﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinancialManager.Entities ;
using System.Data.SqlClient;

namespace FinancialManager.Dal
{
    public class TransactionRepository
    {

        public IList<DtoTransaction>  getTransaction(string isFraud, string datetrx, string accoundest, int pagesize, int pagenumber)
        {
            var result = new List<DtoTransaction>();
            if (string.IsNullOrEmpty(isFraud))       
                isFraud = null;

            if (string.IsNullOrEmpty(datetrx))
                datetrx = null;

            if (string.IsNullOrEmpty(accoundest))
                accoundest = null;

            using (FinancialManagerEntities context = new FinancialManagerEntities())
            {
                 result = context.Database.SqlQuery<DtoTransaction>(" fm_getTransactions {0}, {1}, {2}, {3} , {4} ",
                      isFraud,accoundest, datetrx , pagesize, pagenumber).ToList();

            }
            return result;

        }

        public IList<DtoTransaction> getTransaction(decimal id)
        {
            List<DtoTransaction> result;
            using (FinancialManagerEntities context = new FinancialManagerEntities())
            {
                result = context.Database.SqlQuery<DtoTransaction>(" fm_editTransaction {0}" , id ).ToList();

            }
            return result;

        }

        public void updTransaction(DtoTransaction requestTransaction ) {

            using (FinancialManagerEntities context = new FinancialManagerEntities())
            {
                var trx = context.Transaction.Single(x => x.Id == requestTransaction.Id);
                trx.IsFraud = requestTransaction.isFraud;
                trx.UserModified = requestTransaction.user;
                trx.Modified = requestTransaction.dateTransaction;
                context.SaveChanges();
            }
        }

        public void addTransaction(DtoTransaction requestTransaction){

            Account destine;
            Account origen;

            Transaction transactionAccount = new Transaction();
            transactionAccount.Type = requestTransaction.TypeTx;
            transactionAccount.Origen = requestTransaction.AccountOrigen;            
            transactionAccount.IsFraud = 0;
            transactionAccount.IsFlaggedFraud = 0;
            transactionAccount.User =  requestTransaction.user;
            transactionAccount.Created = requestTransaction.dateTransaction;
            transactionAccount.Value = requestTransaction.Value;

            using (FinancialManagerEntities context = new FinancialManagerEntities())
            {
                
                origen = context.Account.Find(requestTransaction.AccountOrigen);
                transactionAccount.OldBalanceOrigen = origen.Balance;

                if (requestTransaction.TypeTx == "TRANSFER")
                {
                    if (requestTransaction.Value > origen.Balance)
                        throw new Exception("you dont have enough money");
                    
                    var accoutAuthorized = (from x in context.AccontRegister
                                            where x.codAccountOwner == requestTransaction.AccountOrigen &&
                                            x.codAccountAuthorized == requestTransaction.AccountDestine
                                            select x).FirstOrDefault();
                    if (accoutAuthorized == null) {
                        throw new Exception("Account destine is not valid");
                    }

                    destine = context.Account.Find(requestTransaction.AccountDestine);

                    transactionAccount.NewBalanceOrigen = origen.Balance - requestTransaction.Value;
                    transactionAccount.Destination = requestTransaction.AccountDestine;
                    transactionAccount.OldBalanceDestination = destine.Balance;
                    transactionAccount.NewBalanceDestination = destine.Balance + requestTransaction.Value;
                    
                    context.Transaction.Add(transactionAccount);
                    context.Account.Single(x => x.codAcount == origen.codAcount).Balance -= requestTransaction.Value;
                    context.Account.Single(x => x.codAcount == destine.codAcount).Balance += requestTransaction.Value;
                }
                if (requestTransaction.TypeTx == "DEPOSIT")
                {
                    transactionAccount.NewBalanceOrigen = origen.Balance + requestTransaction.Value;
                    context.Transaction.Add(transactionAccount);
                    context.Account.Single(x => x.codAcount == origen.codAcount).Balance += requestTransaction.Value;                    
                }

                if (requestTransaction.TypeTx == "PAYMENT")
                {
                    transactionAccount.NewBalanceOrigen = origen.Balance - requestTransaction.Value;
                    context.Transaction.Add(transactionAccount);
                    context.Account.Single(x => x.codAcount == origen.codAcount).Balance -= requestTransaction.Value;
                }

                context.SaveChanges();
            }

        }
    }

}
