//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FinancialManager.Dal
{
    using System;
    using System.Collections.Generic;
    
    public partial class Transaction
    {
        public decimal Id { get; set; }
        public string Type { get; set; }
        public string Origen { get; set; }
        public decimal OldBalanceOrigen { get; set; }
        public decimal NewBalanceOrigen { get; set; }
        public string Destination { get; set; }
        public Nullable<decimal> OldBalanceDestination { get; set; }
        public Nullable<decimal> NewBalanceDestination { get; set; }
        public int IsFraud { get; set; }
        public int IsFlaggedFraud { get; set; }
        public decimal Value { get; set; }
        public string User { get; set; }
        public System.DateTime Created { get; set; }
        public string UserModified { get; set; }
        public Nullable<System.DateTime> Modified { get; set; }
    }
}
