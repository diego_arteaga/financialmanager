﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinancialManager.Entities ;

namespace FinancialManager.Dal
{
    public class SecurityRepository
    {
        public SecurityRepository()
        {

        }

        public DtoUser getUser(string user, string password)
        {

            DtoUser userlogon = null;
            using (FinancialManagerEntities context = new FinancialManagerEntities())
            {
                var result = ( from  x in context.User
                                   where x.Nickname == user && x.Password == password
                                   select x).FirstOrDefault();

                if (result != null)
                {
                    userlogon = new DtoUser();
                    userlogon.NickName = result.Nickname;
                    userlogon.Tocken = Guid.NewGuid().ToString();
                }

            }
            return userlogon;
        }

        public void addSession(string user, string token)
        {

            DtoUser userlogon = null;
            using (FinancialManagerEntities context = new FinancialManagerEntities())
            {
                var sesion = new Session();
                sesion.User = user;
                sesion.Date = DateTime.Now;
                sesion.Token = token;
                context.Session.Add(sesion);
                context.SaveChanges();
            }
         
        }

        public void delSession(string user, string token)
        {

            DtoUser userlogon = null;
            using (FinancialManagerEntities context = new FinancialManagerEntities())
            {
                var result = (from x in context.Session 
                              where x.User  == user && x.Token  == token 
                              select x).FirstOrDefault();                
                if (result != null)
                {
                    context.Session.Remove(result);
                    context.SaveChanges();
                }
            }

        }

        public DtoUser getTocken(string user, string tocken)
        {
            DtoUser userlogon = null;

            using (FinancialManagerEntities context = new FinancialManagerEntities())
            {
                var result = (from x in context.Session 
                              where x.User  == user && x.Token == tocken
                              select x).FirstOrDefault();

                if (result != null)
                {
                    userlogon = new DtoUser();
                    userlogon.NickName = user;
                    userlogon.Tocken = tocken;
                    userlogon.update = result.Date;
                }

            }
            return userlogon;
        }

        public void updateTocken(DtoUser tokensecurity)
        {
            DtoUser userlogon = null;
            Session result;

            using (FinancialManagerEntities context = new FinancialManagerEntities())
            {
                result = (from x in context.Session
                          where x.User == tokensecurity.NickName && x.Token == tokensecurity.Tocken
                          select x).FirstOrDefault();


                if (result != null)
                {
                    context.Session.Single(x => x.Id == result.Id).Date = tokensecurity.update;
                    context.SaveChanges();

                }
            }

        }

        public string getRole(string user) {
            using (FinancialManagerEntities context = new FinancialManagerEntities())
            {
                return context.User.Single(x => x.Nickname == user).Rol ;
            }
        
        }

    }
}
