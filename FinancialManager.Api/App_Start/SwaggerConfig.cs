using System.Web.Http;
using WebActivatorEx;
using FinancialManager.Api;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace FinancialManager.Api
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                    {   c.SingleApiVersion("v1", "FinancialManager.Api");
                    c.IncludeXmlComments(string.Format(@"{0}\bin\SwaggerSolution.Services.xml",
                       System.AppDomain.CurrentDomain.BaseDirectory));

                    })
                .EnableSwaggerUi(c =>
                    {
                        
                    });
        }
    }
}
