﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Swashbuckle.Application;
using Swashbuckle.Swagger.Annotations;
using FinancialManager.Dal;
using FinancialManager.Entities;
using Halcyon.HAL;
using System.Net.Http.Formatting;
using Newtonsoft.Json.Linq;


namespace FinancialManager.Api.Controllers
{

    [RoutePrefix("api/transaction")]
    public class TransactionController : BaseController 
    {

        [HttpGet]
        [SwaggerOperation("EditTransaction")]
        [Route("EditTransaction")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(string), Description = "string Format Application/hal+json o Application/hal+xml.")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(string), Description = "Server Error. Format Application/problem+json")]
        public IHttpActionResult EditTransactions(decimal id , string user, string tocken)
        {

            try
            {
                IsTokenValid(user, tocken);
                var security = new SecurityRepository();
                var role = security.getRole(user);

                if (role != "ADMINIS" && role != "ASSIS")
                {
                    throw new Exception("this transaction is not available for you");
                }
                var trxrepository = new TransactionRepository();
                var result = trxrepository.getTransaction(id);
                var information = JToken.FromObject(result);
                var answer = new { information };
                return MessageOk(answer);
            }
            catch (Exception ex)
            {
                return MessageError(ex.Message);
            }
        }



        /// <summary>
        /// Register a transaction
        /// </summary>
        /// <param name="accountorigen"></param>
        /// <param name="typeTrx"></param>
        /// <param name="value"></param>
        /// <param name="accountorigen"></param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerOperation("Register") ]
        [Route("Register") ]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(string), Description = "string Format Application/hal+json o Application/hal+xml.")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(string), Description = "Server Error. Format Application/problem+json")]
        public IHttpActionResult RegisterTransaction(string accountorigen, string typeTrx, string accountdestine, string user, string tocken, decimal value )
        {
            try
            {
                var trxbanck = new DtoTransaction()
                {
                    TypeTx = typeTrx,
                    AccountOrigen = accountorigen,
                    AccountDestine = accountdestine,
                    Value = value,
                    user =user ,
                    dateTransaction = DateTime.Now 

                };
                IsTokenValid(user,tocken);
                var security = new SecurityRepository();
                var role = security.getRole(user);

                if (role != "ADMINIS" && role != "ASSIST")
                    throw new Exception("this transaction is not available for you");


                var trxrepository = new TransactionRepository();
                trxrepository.addTransaction(trxbanck);
                var answer = new { message = "successfull" };
                return MessageOk( answer );
            }
            catch (Exception ex)
            {
                return MessageError(ex.Message);
            }

        }


        /// <summary>
        /// Report a transaction as a fraud
        /// </summary>
        /// <param name="accountorigen"></param>
        /// <param name="user"></param>
        /// <param name="tocken"></param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerOperation("ReportFraud")]
        [Route("ReportFraud")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(string), Description = "string Format Application/hal+json o Application/hal+xml.")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(string), Description = "Server Error. Format Application/problem+json")]
        public IHttpActionResult ReportFraud(decimal idtransation, int isfraud, string user, string tocken)
        {
            try
            {
                var trxbanck = new DtoTransaction()
                {
                    Id = idtransation,
                    user = user,
                    dateTransaction = DateTime.Now,
                    isFraud = isfraud 
                };
                IsTokenValid(user, tocken);
                var security = new SecurityRepository();
                var role = security.getRole(user);

                if (role != "ADMINIS" && role != "MANAG")
                    throw new Exception("this transaction is not available for you");

                var trxrepository = new TransactionRepository();
                trxrepository.updTransaction(trxbanck);
                var answer = new { message = "successfull" };
                return MessageOk(answer);
            }
            catch (Exception ex)
            {
                return MessageError(ex.Message);
            }

        }

        /// <summary>
        /// Get information of transaction
        /// </summary>
        /// <param name="isFraud"></param>
        /// <param name="dateTrx"></param>
        /// <param name="accountDest"></param>
        /// <param name="pagezise"></param>
        /// <param name="pagenumber"></param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerOperation("GetTransactions")]
        [Route("GetTransactions")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(string), Description = "string Format Application/hal+json o Application/hal+xml.")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(string), Description = "Server Error. Format Application/problem+json")]        
        public IHttpActionResult getTransactions(int pagezise, int pagenumber , string user, string tocken,  string isFraud ="", string dateTrx ="", string accountDest="" )
        {
            
            try
            {
                IsTokenValid(user, tocken);
                var security = new SecurityRepository();
                var role = security.getRole(user);

                if (role != "ADMINIS" && role != "ASSIS")
                {
                    throw new Exception("this transaction is not available for you");
                }
                var trxrepository = new TransactionRepository();
                var result = trxrepository.getTransaction(isFraud  ,  dateTrx , accountDest, pagezise, pagenumber);
                var information = JToken.FromObject(result);
                var answer = new { information };
                return MessageOk(answer);
            }
            catch (Exception ex)
            {
                return MessageError(ex.Message);
            }
        }

    }
}
