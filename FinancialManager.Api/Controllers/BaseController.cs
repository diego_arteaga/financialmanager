﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Swashbuckle.Application;
using Swashbuckle.Swagger.Annotations;
using Halcyon.HAL;
using System.Net.Http.Formatting;
using Newtonsoft.Json.Linq;
using FinancialManager.Entities;
using FinancialManager.Dal ;



namespace FinancialManager.Api.Controllers
{
    public class BaseController : ApiController
    {

        protected void IsTokenValid(string user, string tocken)
        {

            var security = new SecurityRepository();
            var usertocken = security.getTocken(user, tocken);


            if (usertocken == null)
                throw new Exception("your session is expired");

            if ((DateTime.Now - usertocken.update).TotalMinutes > 15)
            {
                throw new Exception("your session is expired");
            }

            usertocken.update = DateTime.Now;
            security.updateTocken(usertocken);

        }


        protected IHttpActionResult MessageOk(object result)
        {
            JObject element = JObject.FromObject(result);
            var messajeHal = new HALResponse(element);
            var answerOk = new HttpResponseMessage()
            {
                Content = new ObjectContent<HALResponse>(messajeHal, new JsonMediaTypeFormatter(), "application/json"),
                StatusCode = HttpStatusCode.OK
            };

            return ResponseMessage(answerOk);


        }

        protected IHttpActionResult MessageError(string messaje)
        {

            var issue = new DtoError();
            issue.Type = ((int)HttpStatusCode.InternalServerError).ToString();
            issue.Title = Enum.GetName(typeof(HttpStatusCode), HttpStatusCode.InternalServerError);
            issue.Detail = messaje;
            issue.Status = HttpStatusCode.InternalServerError.ToString();
            issue.Instance = SwaggerDocsConfig.DefaultRootUrlResolver(this.Request);
            HttpResponseMessage response = new HttpResponseMessage()
            {
                Content = new ObjectContent<DtoError>(issue, new JsonMediaTypeFormatter()),
                StatusCode = HttpStatusCode.InternalServerError
            };

            return ResponseMessage(response);
        }
    }
}
