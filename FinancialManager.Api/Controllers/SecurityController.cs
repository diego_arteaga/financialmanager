﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Swashbuckle.Application;
using Swashbuckle.Swagger.Annotations;
using FinancialManager.Dal;
using FinancialManager.Entities;
using Halcyon.HAL;
using System.Net.Http.Formatting;
using Newtonsoft.Json.Linq;

namespace FinancialManager.Api.Controllers
{
    [RoutePrefix("api/autenticacion")]
    public class SecurityController : BaseController
    {
        /// <summary>
        /// Allow to authenticate a user 
        /// </summary>
        /// <param name="identification">usuario usado para iniciar sesión</param>
        /// <param name="password">clave del usuario.</param>
        /// <returns>HAL - Swagger Response, result</returns>
        [HttpPost]
        [SwaggerOperation("LogOn")]
        [Route("LogOn")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(string), Description = "string Format Application/hal+json o Application/hal+xml.")]
        [SwaggerResponse(HttpStatusCode.NotFound, Type = typeof(string), Description = "User or password is incorrect . Format Application/problem+json")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(string), Description = "Server Error. Format Application/problem+json")]
        public IHttpActionResult LogOn(string identification, string password)
        {
            try
            {
                DtoUser result;
                var security = new SecurityRepository();
                result = security.getUser(identification, System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(password)));
                security.addSession(identification, result.Tocken);
                return MessageOk(result);
            }
            catch (Exception ex)
            {
                return MessageError(ex.Message);
            }


        }

        /// <summary>
        /// Finish session
        /// </summary>
        /// <param name="identification">user identification</param>
        /// <param name="token">Security Tocken.</param>
        /// <returns>HAL - Swagger Response, result</returns>
        [HttpPost]
        [SwaggerOperation("LogOff")]
        [Route("LogOff")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(string), Description = "string Format Application/hal+json o Application/hal+xml.")]
        [SwaggerResponse(HttpStatusCode.NotFound, Type = typeof(string), Description = "Session not valid . Format Application/problem+json")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(string), Description = "Server Error. Format Application/problem+json")]
        public IHttpActionResult LogOff(string identification, string tocken)
        {
            try
            {
                DtoUser result;
                var security = new SecurityRepository();
                security.delSession(identification, tocken);
                var answer = new { message = "successfull" };
                return MessageOk(answer);
            }
            catch (Exception ex)
            {
                return MessageError(ex.Message);
            }


        }





    }
}

