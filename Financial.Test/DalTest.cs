﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FinancialManager.Dal;

namespace FinancialManager.Test
{
    [TestClass]
    public class DalTest
    {
        [TestMethod]
        public void addCustomer()
        {

            var context = new FinancialManagerEntities();
            var result = context.User.Find("dfalopez");
            if (result != null)
                context.User.Remove(result);
            var user = new User();
            user.Nickname = "dfalopez";
            user.Name = "Diego Arteaga";
            user.Email  = "dfalopez@gmail.com";
            user.Rol  = "ADMIN";            
            user.Password = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("dfalopez"));
            context.User.Add(user);
            context.SaveChanges();
            Assert.IsTrue(true);
            
        }

       

        [TestMethod]
        public void delCustomer()
        {

            var context = new FinancialManagerEntities();
            var result = context.Customer.Find("5206779");
            if (result != null)
            {
                context.Customer.Remove(result);
                context.SaveChanges();

            }
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void getUser()
        {

            var context = new SecurityRepository();
            var resultado = context.getUser("dfalopez", "ZGZhbG9wZXo=");
            Assert.IsTrue(true);
        }
    }
}
