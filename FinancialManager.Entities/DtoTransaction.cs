﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinancialManager.Entities
{
    public class DtoTransaction
    {
        public decimal Id { get; set; }
        public string AccountOrigen { get; set; }
        public string AccountDestine { get; set; }
        public string TypeTx { get; set; }
        public decimal  Value { get; set; }
        public int isFraud { get; set; }
        public DateTime dateTransaction { get; set; }
        public string user { get; set; }
        public decimal OldBalanceOrigen { get; set; }
        public decimal NewBalanceOrigen { get; set; }

    }
}
